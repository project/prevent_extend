CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This simple module hides the "Extend" Admin toolbar menu item and returns a
403 access denied for all the module install, uninstall, update and list paths.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/prevent_extend

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/prevent_extend


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommended modules

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * John Burch (webfaqtory) - https://www.drupal.org/u/webfaqtory

This project has been sponsored by:
 * United Nations International Computing Centre
