<?php

namespace Drupal\prevent_extend\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class PreventExtendSubscriber.
 *
 * @package Drupal\prevent_extend\EventSubscriber
 */

class PreventExtendSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public function checkForModuleInstallUninstall(\Symfony\Component\HttpKernel\Event\RequestEvent $event) {
    $request = $event->getRequest();
    if ($request->attributes->get('_route') == 'update.module_install' || $request->attributes->get('_route') == 'system.modules_uninstall' || $request->attributes->get('_route') == 'update.module_update' || $request->attributes->get('_route') == 'system.modules_list') {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['checkForModuleInstallUninstall'];
    return $events;
  }

}
